package ttu.bakatoo.facebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;
import ttu.bakatoo.user.User;
import ttu.bakatoo.user.UserRepository;
import ttu.bakatoo.user.UserService;

// võimaldab autowireda
@Service
public class FacebookConnectionSignup implements ConnectionSignUp {

    private UserService userService;

    public FacebookConnectionSignup(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute(Connection<?> connection) {

        if (!userService.findByFacebookId(connection.getKey().getProviderUserId()).isPresent()) {
            userService.saveUser(new User(connection.getKey().getProviderUserId()));
        }
        return connection.getKey().getProviderUserId();
    }
}
