package ttu.bakatoo.submittedtest;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ttu.bakatoo.test.TestName;
import ttu.bakatoo.user.User;

import java.util.List;

public interface SubmittedTestRepository extends CrudRepository<SubmittedTest, Long> {

    List<SubmittedTest> findAllByUser(User user);

    @Query("SELECT ST FROM SubmittedTest ST " +  // ST on alias
        "WHERE ST.test.name = :testName " +
        "ORDER BY ST.submitDateTime")
    List<SubmittedTest> findAllByTestName(@Param("testName") TestName testName); // annan aliase "testName", et querys kasutada





}
