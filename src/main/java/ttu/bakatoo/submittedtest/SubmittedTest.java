package ttu.bakatoo.submittedtest;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.submittedanswer.SubmittedAnswer;
import ttu.bakatoo.submittedquestion.SubmittedQuestion;
import ttu.bakatoo.test.Test;
import ttu.bakatoo.user.User;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SubmittedTest {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private User user;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "test", referencedColumnName = "id")
    private Test test;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "rel_submitted_test_submitted_question",
            joinColumns=@JoinColumn(name="submitted_test_id", referencedColumnName = "id"),
            inverseJoinColumns=@JoinColumn(name="submitted_question_id", referencedColumnName = "id"))
    private List<SubmittedQuestion> submittedQuestions;

    @Column(name = "submit_date_time", updatable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDateTime submitDateTime;

    public SubmittedTest(User user, Test test, List<SubmittedQuestion> submittedQuestions) {
        this.user = user;
        this.test = test;
        this.submittedQuestions = submittedQuestions;
    }

    public long getCorrectAnswerCounter() {
        return submittedQuestions.stream()
                .map(submittedQuestion -> submittedQuestion.isCorrectlyAnswered() ? 1 : 0)
                .reduce(0, (i,j) -> i+j);
    }

    public long getWrongAnswerCounter() {
        return submittedQuestions.size()-getCorrectAnswerCounter();
    }

    public String getFormattedSubmitDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return this.getSubmitDateTime().format(formatter);
    }

    // salvestades tehakse enne see meetod
    @PrePersist
    public void setSubmitDateTime() {
        this.submitDateTime = LocalDateTime.now();
    }

}
