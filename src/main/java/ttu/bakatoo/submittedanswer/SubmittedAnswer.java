package ttu.bakatoo.submittedanswer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.option.Options;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SubmittedAnswer {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "option", referencedColumnName = "id")
    private Options option;

    private boolean guess;

    // lihtsustab õige ja vale näitamist UI-s ilusamaks, siin salvestan kas "option" ja "guess" matchivad
    private boolean guessIsCorrect;

}
