package ttu.bakatoo.submittedanswer;

import org.springframework.data.repository.CrudRepository;

public interface SubmittedAnswerRepository extends CrudRepository<SubmittedAnswer, Long> {
}
