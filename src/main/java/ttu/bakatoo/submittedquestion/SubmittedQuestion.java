package ttu.bakatoo.submittedquestion;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.option.Options;
import ttu.bakatoo.question.Question;
import ttu.bakatoo.submittedanswer.SubmittedAnswer;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SubmittedQuestion {

    @Id
    @GeneratedValue
    private long id;

    @OneToOne()
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "rel_submitted_question_submitted_answer",
            joinColumns = @JoinColumn(name="submitted_question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name="submitted_answer_id", referencedColumnName = "id"))
    private List<SubmittedAnswer> submittedAnswers;

    public SubmittedQuestion(Question question, List<SubmittedAnswer> submittedAnswers) {
        this.question = question;
        this.submittedAnswers = submittedAnswers;
    }

    public boolean isCorrectlyAnswered() {
        return this.getSubmittedAnswers().stream()
                .allMatch(SubmittedAnswer::isGuessIsCorrect);
    }

}
