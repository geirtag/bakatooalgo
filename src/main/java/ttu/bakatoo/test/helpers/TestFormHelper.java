package ttu.bakatoo.test.helpers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.question.Question;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class TestFormHelper {

    private long testId;

    private List<TestFormQuestionHelper> questions;

    // id annan ette, võtan selle testiga seotud küsimused
    public TestFormHelper(long testId, List<Question> questions) {
        this.testId = testId;
        this.questions = questions.stream()
                .map(TestFormQuestionHelper::new)
                .collect(Collectors.toList());
    }
}
