package ttu.bakatoo;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import ttu.bakatoo.bootstrap.Bootstrap;

// EntityScan for JSR310 is needed to support java.util.LocalDate in Entity classes,
@EntityScan(basePackageClasses = { BakatooApplication.class, Jsr310JpaConverters.class })
@SpringBootApplication
public class BakatooApplication {

	public static void main(String[] args) {
		SpringApplication.run(BakatooApplication.class, args);
	}

	@Bean
	public ApplicationRunner initialize() {
		return new Bootstrap();
	}
}
