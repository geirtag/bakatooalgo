package ttu.bakatoo.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ttu.bakatoo.submittedquestion.SubmittedQuestion;
import ttu.bakatoo.submittedtest.SubmittedTest;
import ttu.bakatoo.submittedtest.SubmittedTestRepository;
import ttu.bakatoo.test.Test;
import ttu.bakatoo.test.TestName;
import ttu.bakatoo.test.TestRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    TestRepository testRepository;

    @Autowired
    SubmittedTestRepository submittedTestRepository;

    private static final int QUESTIONS_PER_TEST = 10;

    public List<Question> findRandomTen(TestName name) {

        List<SubmittedQuestion> submittedQuestions = submittedTestRepository.findAllByTestName(name).stream()
                .map(SubmittedTest::getSubmittedQuestions)
                .flatMap(Collection::stream)
                .collect(Collectors.toList()); // tagatan listi kõikide kasutaja submititud questionitest

        List<Long> allTestQuestionIds = testRepository.findByName(name).getQuestions().stream() // k6ik questionid koos v6imalike duplikaatidega
                .map(Question::getId)
                .collect(Collectors.toList());

        for (SubmittedQuestion submittedQuestion : submittedQuestions) {
            boolean correct = submittedQuestion.isCorrectlyAnswered();

            long currentQuestionId = submittedQuestion.getQuestion().getId();

            if (correct) {
                if (allTestQuestionIds.stream().filter(id -> id == currentQuestionId).count() > 1) {
                    allTestQuestionIds.remove(currentQuestionId);
                }
            } else {
                allTestQuestionIds.add(currentQuestionId);
            }
        }

        List<Long> tenRandomIds = new ArrayList<>();
        while (tenRandomIds.size() < QUESTIONS_PER_TEST) {
            long rand = new Random().nextInt(allTestQuestionIds.size());
            if (!tenRandomIds.contains(allTestQuestionIds.get((int)rand))) {
                tenRandomIds.add(allTestQuestionIds.get((int)rand));
            }
        }

        return tenRandomIds.stream()
                .map(id -> questionRepository.findOne(id))
                .collect(Collectors.toList());
    }

    public Question findOne(long id) {
        return questionRepository.findOne(id);
    }
}
