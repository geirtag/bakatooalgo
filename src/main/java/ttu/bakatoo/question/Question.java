package ttu.bakatoo.question;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.option.Options;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue
    private long id;
    private String text;


    @OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "rel_question_options",
        joinColumns = @JoinColumn(name="question_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name="options_id", referencedColumnName = "id"))
    private List<Options> options;

    public Question(String text) {
        this.text = text;
    }


    public Question(String text, List<Options> options) {
        this.text = text;
        this.options = options;
    }

}
